const zedorpc = require('zedo-rpc');

let zedo = new zedorpc.ZedoRPC();

zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());


async function main()
{
    return zedo.AllPulsersOff()
    .then((res) => {
        return zedo.SetPulser("1.0B", { "pulser_mode":"sin", "offset_mode":"+15V", "amp_v":10, "freq_hz":110000} )
        .then((res) => console.log(res));
    })
}
