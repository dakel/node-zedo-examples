const zedorpc = require('zedo-rpc');

function print_json(obj) {
    console.dir(obj, { "depth":null, "colors": true }); 
    return obj;
}

/////////////////////////////////////////////////////////////////////////////////////

let zedo = new zedorpc.ZedoRPC();
zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());

async function main()
{
    // this is just an example, verbosity="min" only to get configuration status
    return zedo.Configure({ "name": "65.8A", "gain_amp": 20 }, "min")
        .then((res) => {
            print_json(res);
        })
}

