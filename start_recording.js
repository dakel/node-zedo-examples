const zedorpc = require('zedo-rpc');

function print_json(obj) {
    console.dir(obj, { "depth":null, "colors": true }); 
    return obj;
}

/////////////////////////////////////////////////////////////////////////////////////

let zedo = new zedorpc.ZedoRPC();
zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());

async function main()
{
    // this is just an example
    return zedo.StartRecording("MyFolder/MyName", 5)
        .then((res) => {
            print_json(res);
        })
}

