const zedorpc = require('zedo-rpc');
const inquirer = require('inquirer');
const util = require('util');
const F = util.format;

/////////////////////////////////////////////////////////////////////////////////////
// application code 

let zedo = new zedorpc.ZedoRPC();
zedo.connect()
    .then(main)
    .catch((err) => console.log("Something has failed: " + err.message || err.toString()))
    .finally(() => zedo.close());

/////////////////////////////////////////////////////////////////////////////////////
// utility functions

function print_json(obj) {
    console.dir(obj, { "depth":null, "colors": true }); 
    return obj;
}

/////////////////////////////////////////////////////////////////////////////////////
// application functions

async function prompt_options(target, msg, member, current, options)
{
    let dflt = null;
    var answer;
    var a;

    console.log(msg);

    for(i=1; i<=options.length; i++)
    {
        let entry = i.toString() + ": " + options[i-1];

        if(current && current[member] === options[i-1])
        {
            dflt = i;
            entry = entry + " (current)";
        }

        console.log("    " + entry);
    }

    if(dflt)
        msg = F('select 1-%d [default:%d]', options.length, dflt);
    else
        msg = F('select 1-%d', options.length);

    do
    {
        answer = await inquirer.prompt({ name:'value', message: msg });
        a = Number.parseInt(answer.value);

        if(dflt && !Number.isInteger(a))
            a = dflt;
            
    } while(!Number.isInteger(a) || a <= 0 || a > options.length);

    a = options[a-1].split(" ")[0];

    target[member] = a;
    return a;
}

async function prompt_number(target, msg, member, current, factor=1)
{
    dflt = current ? current[member] : null;

    if(Number.isFinite(dflt))
    {
        dflt /= factor;
        msg = msg + F(" [default:%s]", dflt.toString());
    }

    do 
    {
        answer = await inquirer.prompt({ name:'value', message: msg });
        a = Number.parseFloat(answer.value);

        if(!Number.isFinite(a))
            a = dflt;
        
    } while(!Number.isFinite(a));

    target[member] = a * factor;
    return a;
}

async function prompt_pulser_config(target, puls_current)
{
    m = await prompt_options(target, "Pulser mode:", "pulser_mode", puls_current, ["off", "sin", "sweep", "burst", "bus", "bnc", "passive"]);

    if(m != "off")
    {
        await prompt_options(target, "Offset mode:", "offset_mode", puls_current, ["+15V", "0V", "-5V", "-15V"]);

        if(m == "sin" || m == "sweep" || m == "burst") {
            await prompt_number(target, "Enter freq in kHz", "freq_hz", puls_current, 1000);

            if(m == "sweep") {
                await prompt_number(target, "Enter maximum sweep freq in kHz", "fmax_hz", puls_current, 1000);
                await prompt_number(target, "Enter sweep time in seconds", "sweep_sec", puls_current);
                await prompt_number(target, "Enter number of sweep steps", "sweep_steps", puls_current);
            }
            else if(m == "burst") {
                //await prompt_options(target, "Enter burst shape", "burst_shape", puls_current, ["rect"]);
                await prompt_number(target, "Enter burst length in milliseconds", "burst_len_sec", puls_current, 0.001);
                await prompt_number(target, "Enter burst repeat time in milliseconds", "burst_rep_sec", puls_current, 0.001);
            }
        }
    }
}

async function main()
{
    resp = await zedo.call("GetSensors", null);

    if(!resp.items || resp.items.length == 0)
    {
        console.log("No sensors found, exit");
        return;
    }

    let sensors = new Array();
    let config = { name: "", type: "bunit", pulser: {} };
    let pulser_owner = null;

    for(i=0; i<resp.items.length; i++)
    {
        let item = resp.items[i];
        let name = item.name;

        if(item.pulser.pulser_mode != "off")
        {
            if(item.pulser.pulser_mode == "passive")
            {
                name = name + " (passive)";
            }
            else
            {
                pulser_owner = item;
                name = name + " (current owner)";
            }
        }

        sensors.push(name);
    }

    if(!sensors.length)
        return null;
        
    await prompt_options(config, "Select the sensor index to switch as pulser:", "name", null, sensors)
    await prompt_pulser_config(config.pulser, pulser_owner ? pulser_owner.pulser : null);

    // deactivate existing first
    if(pulser_owner && pulser_owner.name != config.name)
    {
        let deactivate = { "name": pulser_owner.name, "type": "bunit", "pulser": { "pulser_mode":"off"} };
        await zedo.call("Configure", { "items": [ deactivate ]})
            .then( (ok) => console.log(F("Deactivated %s", pulser_owner.name)))
            .catch( (err) => console.log("Deactivation Failed " + err.toString()));
    }    

    // configure and return the promise
    return zedo.call("Configure", { "items": [ config ]})
        .then( (ok) => console.log(F("Activated %s to %s", ok.items[0].name, ok.items[0].pulser.pulser_mode)))
        .catch( (err) => console.log("Activation Failed " + err.toString()));
}

