const zedorpc = require('zedo-rpc');

let zedo = new zedorpc.ZedoRPC();

zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());


async function main()
{
    return zedo.AllPulsersOff()
    .then((res) => {
        console.log("These pulsers were turned off:");
        console.log(zedo.get_names(res));
    })
}
