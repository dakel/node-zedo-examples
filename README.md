## node-zedo-examples - node.js examples on using node-zedo-rpc library

### Overview 
The **node-zedo-rpc** module implements the [ZedoRPC][1] class which can be used to simplify a JSON-RPC access to Dakel ZEDO system for Acoustic Emission measurements. Dakel ZEDO is an advanced AE measurement system scalable from a single channel to hundreds of channels enabling AE signal acquisition and analysis, AE hit detection, AE events localization and more.

Dakel ZEDO is controlled by a software called ZDaemon (ZEDO Diagnostic AE Monitor) which runs on a Windows-based PC with rich graphical interface. The ZDaemon exports many services over a JSON-RPC API which can be accessed from external applications coded in any programming language. 

This repository contains various examples of using node.js and JScript language to access the ZEDO and ZDaemon features.

[1]: https://bitbucket.org/dakel/node-zedo-rpc
