const zedorpc = require('zedo-rpc');

let zedo = new zedorpc.ZedoRPC();

zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());

async function main()
{
    return zedo.GetActivePulsers()
    .then((res) => {
        console.log("Active pulsers:");
        console.log(zedo.get_names(res));
    })
}
