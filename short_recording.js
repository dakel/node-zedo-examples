const zedorpc = require('zedo-rpc');

const DURATION_SEC = 300;
const BASE_PATH   = "C:/ZEDO_AUTO/";
const DEFAULT_DIR = "nepojmenovane";

/////////////////////////////////////////////////////////////////////////////////////

let zedo = new zedorpc.ZedoRPC();
zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());

async function main()
{
    let dir = process.argv.length >= 3 ? process.argv[2] : DEFAULT_DIR;
    let path = BASE_PATH + dir;

    console.log("Recording in " + path);

    // this is just an example
    return zedo.StartRecording(path, 1)
        .then((res) => zedo.delay(DURATION_SEC*1000))
            .then((res) => zedo.StopRecording())
                .then((res) => zedo.print_json(res));
}

