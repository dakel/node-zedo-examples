const zedorpc = require('zedo-rpc');

function print_json(obj) {
    console.dir(obj, { "depth": null, "colors": true });
    return obj;
}

const THE_ITEM = "LG1";

// TODO: specify base directory for data text export (data sub-directories will be created)
const EXPORT_OUTPATH = "c:\\ZEDO_Data\\__test_json_export\\export.lg";

// TODO: specify export configuration. See help of ZedoRPC.ExportFileReaderData
const EXPORT_CFG = {
    export: {
        allow: ["l_locevn", "l_locfilt"],
        mode: 2,           // Export mode: 0=do nothing, validate config only, 1=show export dialog, 2=start background export job. Default:0.
    },

    time: {
        base: 0,            // Time base for relative time calculation. One of the following
        //   - 0 ... time is relative to FileReader base time (Default)
        //   - 1 ... relative to the first point ever seen
        //   - Number ... specific JavaScript time is used as a base time
        time_format: 0,     // Time format. 0=default relative time in seconds,  1=nanosecond integer time. Default:0
        date_format: 1,     // Absolute time/date format. 0=not exported, 1=Date/Time, 2=Time only. Default:0
    },

    siginfo: {
        voltage_format: 5,  // AE voltage in Volts: 0=not exported, 1=(int)nV, 2=(int)uV, 3=(float)uV, 4=(float)mV, 5=(float)V. Default=5.
        voltage_dbae: 1,    // AE voltage in dBAE: 0=not exported, 1=exported. Default=0.
        counts_log: 1,      // AE counts in logarithmic units: 0=not exported, 1=exported. Default=1.
        counts_lin: 1,      // AE counts in linear units: 0=not exported, 1=exported. Default=0.
        energy_format: 1,   // AE energy units: 0=V^2/Hz, 1=uV^2/Hz. Default=1.
    },
};

/////////////////////////////////////////////////////////////////////////////////////

let zedo = new zedorpc.ZedoRPC({ required_version: 502 });

zedo.connect()
    .then(main)
    .catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
    .finally(() => zedo.close());

async function main() {
    return zedo.GetItemInfo(THE_ITEM)
        .then((res) => {
            print_json(res);

            if (res.status == 0)
                return zedo.WaitItemsIdle(THE_ITEM)
            else
                throw "Error getting the item";
        })
        .then((res) => {
            if (res.status == 0) {
                print_json(res.items);
                return zedo.ExportItems(THE_ITEM, EXPORT_OUTPATH, "one", EXPORT_CFG, true);
            } else {
                print_json(res);
                throw "Error waiting for item";
            }
        })
        .then((result) => {
            if (result.hasOwnProperty("job_id")) {
                console.log(`Waiting for export job id: ${result.job_id}`);

                return zedo.ExportJobProgressMonitor(result.job_id, 1, 200, (jobstate) => {
                    console.log(` - job ${jobstate.job_id} progress=${jobstate.progress.toFixed(2)}%, time=${jobstate.time_running.toFixed(2)}`);
                    return false; // keep monitor probing
                })
                    .then((done) => {
                        console.log(`Export (job_id=${done.job_id}) ` + ((done.done) ? "finished" : "NOT-YET FINISHED"));

                        // print details
                        console.log("output files:");
                        done.out_files.forEach((f) => console.log(" - " + f));
                        console.log("errors:");
                        done.errors.forEach((f) => console.log(" - " + f));
                        console.log("===============");
                    })
                    .catch((err) => {
                        console.log("Export failed: " + err);
                    });
            }
            else {
                console.log("Job NOT started");
                print_json(result);
                return true;
            }
        })
}
