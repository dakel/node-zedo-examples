const zedorpc = require('zedo-rpc');

let zedo = new zedorpc.ZedoRPC();
zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());

async function main()
{
    return zedo.GetSystemTime()
        .then((res) => {
            console.log("Global nanosecond time: " + res.gtime + 
            " Local time: " + res.local_date + " "  + res.local_time);
        })
}

