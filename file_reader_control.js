// This example walks a base directory with ZEDO data sub-folders, it opens and exports each of them

const zedorpc = require('zedo-rpc');
const fs = require('fs');
const pathlib = require("path");

/////////////////////////////////////////////////////////////////////////////////////
// globals

// TODO: specify name of FileReader. It will be automatically created if not yet exists
const READER_NAME = "my_reader";

// TODO: specify base directory of multiple ZEDO data folders
const INPUTPATH = "c:\\ZEDO_Data\\MULTIPLE_DATA_FOLDER";

// TODO: specify base directory for picture export (data sub-directories will be created)
const PICT_OUTPATH = "c:\\ZEDO_Data\\Exported_Pictures";

// TODO: specify base directory for data text export (data sub-directories will be created)
const EXPORT_OUTPATH = "c:\\ZEDO_Data\\Exported_Data";

// TODO: specify export configuration. See help of ZedoRPC.ExportFileReaderData
const EXPORT_CFG = {
    export: {
        allow: [ "r_siginfo", "r_hitdet0", "r_sigsmp", "r_fastrms" ],
        mode: 2,           // Export mode: 0=do nothing, validate config only, 1=show export dialog, 2=start background export job. Default:0.
    },

    time: {
        base: 0,            // Time base for relative time calculation. One of the following
                            //   - 0 ... time is relative to FileReader base time (Default)
                            //   - 1 ... relative to the first point ever seen
                            //   - Number ... specific JavaScript time is used as a base time
        time_format: 0,     // Time format. 0=default relative time in seconds,  1=nanosecond integer time. Default:0
        date_format: 1,     // Absolute time/date format. 0=not exported, 1=Date/Time, 2=Time only. Default:0
    },

    siginfo: {
        voltage_format: 5,  // AE voltage in Volts: 0=not exported, 1=(int)nV, 2=(int)uV, 3=(float)uV, 4=(float)mV, 5=(float)V. Default=5.
        voltage_dbae: 1,    // AE voltage in dBAE: 0=not exported, 1=exported. Default=0.
        counts_log: 1,      // AE counts in logarithmic units: 0=not exported, 1=exported. Default=1.
        counts_lin: 1,      // AE counts in linear units: 0=not exported, 1=exported. Default=0.
        energy_format: 1,   // AE energy units: 0=V^2/Hz, 1=uV^2/Hz. Default=1.
    },
};

/////////////////////////////////////////////////////////////////////////////////////
// the code

let zedo = new zedorpc.ZedoRPC( { required_version: 491 } );

zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());

async function main()
{
    // first, retrieve the default FileReader object
    await zedo.OpenFileReaderByName(READER_NAME)
    .then((res) => {
        if(!res.status) {
            // start recursion at the base directory
            return process_directory(res._id, INPUTPATH);
        } else {
            throw "Cannot locate default File Reader";
        }
    })
}

async function process_directory(reader_id, dir) {
    var stat = fs.lstatSync(dir);

    // only process the path if it is a directory
    if(stat.isDirectory())
    {
        // if it is ZEDO data dir
        if(zedo.is_zdat_directory(dir)) {
            // export pictures and do not recurse
            console.log("Processing " + dir);
            await export_one(reader_id, dir);
        }
        else {
            // normal directory - recurse to all subdirectories
            var files = fs.readdirSync(dir);
            for(var i=0; i<files.length; i++) {
                await process_directory(reader_id, dir + "\\" + files[i]);
            }
        }
    }
}

async function export_one(reader_id, path) {
    var reader_info;

    return zedo.SetFileReaderPath(reader_id, path, false)
    .then((res) => {
        if(!res.status)
            return zedo.WaitFileReaderScanned(reader_id);
        else
            throw "Error when waiting for file reader data";
    })
    .then((res) => {
        if(!res.status)
            return zedo.GetFileReaderInfo(reader_id);
        else
            throw "Failed to wait for filereader data";
    })
    .then((reader_info) => {
        if(!reader_info.status) {
            if(PICT_OUTPATH) {
                console.log(`Exporting currently open pictures to ${PICT_OUTPATH}`);
                return zedo.CaptureGraphPictures(PICT_OUTPATH, pathlib.basename(path), true, false).then(() => { return reader_info; });
            }
            else
                return reader_info;
        }
        else
            throw "Could not obtain FileReader info";
    })
    .then((reader_info) => {
        if(!reader_info.status) {
            if(EXPORT_OUTPATH) {
                console.log(`Exporting data to ${EXPORT_OUTPATH}`);
                return zedo.ExportFileReaderData(reader_id, EXPORT_OUTPATH, pathlib.basename(path), EXPORT_CFG, true);
            }
            else
                return { "status": 0 };
        }
        else
            throw "No error";
    })
    .then((result) => {
        if(result.hasOwnProperty("job_id")) {
            console.log(`Waiting for export job id: ${result.job_id}`);
            return zedo.ExportJobProgressMonitor(result.job_id, 1, 0, (jobstate) => {
                    console.log(` - job ${jobstate.job_id} progress=${jobstate.progress.toFixed(2)}%, time=${jobstate.time_running.toFixed(2)}`);
                    return false; // keep monitor probing
                })
                .then((done) => {
                    console.log(`Export (job_id=${done.job_id}) ` + ((done.done) ? "finished" : "NOT-YET FINISHED"));
                    console.log(` - output files: ${done.out_files.length}  errors: ${done.errors.length}`);
                })
                .catch((err) => {
                    console.log("Export failed: " + err);
                });
        }
        else {
            return true;
        }
    });
}
