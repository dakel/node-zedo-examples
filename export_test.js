// This example reads a content of a file reader and exports data selected by JSONPath Query

const zedorpc = require('zedo-rpc');
const fs = require('fs');
const pathlib = require("path");
const jsonpath = require('jsonpath');

/////////////////////////////////////////////////////////////////////////////////////
// globals

// TODO: specify name of FileReader which exists in ZDaemon with some valid data
const READER_NAME = "my_reader";

// TODO: specify base directory for picture export (data sub-directories will be created)
const PICT_OUTPATH = "c:\\ZEDO_Export\\my_reader\\Exported_Pictures";

// TODO: specify base directory for data text export (data sub-directories will be created)
const EXPORT_OUTPATH = "c:\\ZEDO_Export\\my_reader\\Exported_Data";

// TODO: enable detailed report at the end
const DETAILED_REPORT = 0;

// jsonpath query syntax: https://www.npmjs.com/package/jsonpath
// TODO: modify the queries according to your data
const EXPORT_QUERY = [
    // select the SIGSMP (AE signal) data from unit 17.0A
    '$.data[?(@.type=="r_bunit" && @.name=="17.0A")].data[?(@.type=="r_sigsmp")]',

    // select FASTRMS and SIGSMP data from unit 17.0B
    '$.data[?(@.type=="r_bunit" && @.name=="17.0B")].data[?(@.type=="r_fastrms" || @.type=="r_sigsmp")]',
];

// TODO: specify export configuration. See help of ZedoRPC.ExportFileReaderData
const EXPORT_CFG = {
    export: {
        allow: [],          // will be filled in runtime by executing EXPORT_QUERY
        mode: 2,            // Export mode: 0=do nothing, validate config only, 1=show export dialog, 2=start background export job. Default:0.
    },

    time: {
        base: 0,            // Time base for relative time calculation. One of the following
                            //   - 0 ... time is relative to FileReader base time (Default)
                            //   - 1 ... relative to the first point ever seen
                            //   - Number ... specific JavaScript time is used as a base time
        time_format: 0,     // Time format. 0=default relative time in seconds,  1=nanosecond integer time. Default:0
        date_format: 1,     // Absolute time/date format. 0=not exported, 1=Date/Time, 2=Time only. Default:0
    },

    stream:                   // Format of general stream data (FastRMS, Analog input, ...). Note that sigsmp has custom settings.
    {
        file_format: 0,        // 0=text only, 1=text+binary, Default=0.
    },

    sigsmp: {
        mode: 1,            // AE Signal export mode: 0:hits only, 1:countinuous stream, 2:spectrogram. Default:1.
        voltage_format: 5,  // AE voltage in Volts: 0=not exported, 1=(int)nV, 2=(int)uV, 3=(float)uV, 4=(float)mV, 5=(float)V. Default=5.

        output: {
            file_format: 0, // 0=text+binary, 1=text+text, 2=single text. Default=0.
            psd_format: 1,  // Generate spectrum file: 0=not exported, 1=exported. Default=0.
        }
    },

    siginfo: {
        voltage_format: 5,  // AE voltage in Volts: 0=not exported, 1=(int)nV, 2=(int)uV, 3=(float)uV, 4=(float)mV, 5=(float)V. Default=5.
        voltage_dbae: 1,    // AE voltage in dBAE: 0=not exported, 1=exported. Default=0.
        counts_log: 1,      // AE counts in logarithmic units: 0=not exported, 1=exported. Default=1.
        counts_lin: 1,      // AE counts in linear units: 0=not exported, 1=exported. Default=0.
        energy_format: 1,   // AE energy units: 0=V^2/Hz, 1=uV^2/Hz. Default=1.
    },
};

/////////////////////////////////////////////////////////////////////////////////////
// the code

let zedo = new zedorpc.ZedoRPC( { required_version: 491 } );

zedo.connect()
.then(main)
.catch((err) => console.log("Something has failed: " + err + " (msg=" + err.message + ")"))
.finally(() => zedo.close());

async function main()
{
    // first, retrieve the FileReader object
    await zedo.OpenFileReaderByName(READER_NAME)
    .then((res) => {
        if(!res.status) {
            return export_filereader(res._id);
        } else {
            throw "Cannot locate default File Reader";
        }
    })
}

async function export_filereader(reader_id) {

    return zedo.WaitFileReaderScanned(reader_id)
    .then((res) => {
        if(!res.status)
            return zedo.GetFileReaderInfo(reader_id);
        else
            throw "Failed to wait for filereader data";
    })
    .then((reader_info) => {
        if(!reader_info.status) {
            if(PICT_OUTPATH) {
                console.log(`Exporting currently open pictures to ${PICT_OUTPATH}`);
                return zedo.CaptureGraphPictures(PICT_OUTPATH, "pictures", true, false).then(() => { return reader_info; });
            }
            else
                return reader_info;
        }
        else
            throw "Could not obtain FileReader info";
    })
    .then((reader_info) => {
        if(!reader_info.status) {
            if(EXPORT_OUTPATH) {
                console.log(`Exporting data to ${EXPORT_OUTPATH}`);
                // evaluate results of all queries and add them to allow list
                EXPORT_QUERY.forEach((q) => {
                    let allow_ids = jsonpath.query(reader_info, q).map((v) => {
                        console.log(` - will export: '${v.name}'`);
                        return `_id:${v._id}`;
                    });
                    EXPORT_CFG.export.allow = EXPORT_CFG.export.allow.concat(allow_ids);
                })
                return zedo.ExportFileReaderData(reader_id, EXPORT_OUTPATH, "text", EXPORT_CFG, true);
            }
            else {
                return { "status": 0 };
            }
        }
        else {
            throw "No error";
        }
    })
    .then((result) => {
        if(result.hasOwnProperty("job_id")) {
            console.log(`Waiting for export job id: ${result.job_id}`);

            return zedo.ExportJobProgressMonitor(result.job_id, 1, 60, (jobstate) => {
                    console.log(` - job ${jobstate.job_id} progress=${jobstate.progress.toFixed(2)}%, time=${jobstate.time_running.toFixed(2)}`);
                    return false; // keep monitor probing
                })
                .then((done) => {
                    console.log(`Export (job_id=${done.job_id}) ` + ((done.done) ? "finished" : "NOT-YET FINISHED"));

                    if(DETAILED_REPORT) {
                        // print details
                        console.log("output files:");
                        done.out_files.forEach((f) => console.log(" - " + f));
                        console.log("errors:");
                        done.errors.forEach((f) => console.log(" - " + f));
                        console.log("===============");
                    } else {
                        // print statistics
                        console.log(`output files: ${done.out_files.length}  errors: ${done.errors.length}`);
                    }
                })
                .catch((err) => {
                    console.log("Export failed: " + err);
                });
        }
        else {
            return true;
        }
    });
}
